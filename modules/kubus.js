const fs = require('fs');
const data = require('../data/data.json')

let luaskubus=(sisi)=>{
    const nilailuaskubus=(6*sisi*sisi);
    simpandatakubus(nilailuaskubus,'Luas','Kubus')
    return nilailuaskubus;
}

let volumekubus=(sisi)=>{
    const nilaivolumekubus=(sisi*sisi*sisi);
    simpandatakubus(nilaivolumekubus,'Volume','Bola')
    return nilaivolumekubus;
}


let simpandatakubus = (nilai,tipe,jenis) =>{
    let formdata={id:data.length,nilai,tipe,jenis}
    let newData = data;
    newData.push(formdata);
    console.log(newData);
    fs.writeFile('./data/data.json', JSON.stringify(newData), "utf-8", (err) => {
        if(err) throw err;
        console.log('Data Successfully Saved');
    });
};

const editdatakubus=(param)=>{
  const{id, payload}=param;
  let newData=data;
  const index=newData.findIndex((row)=>row.id === id);

  newData[index]={
    ...newData[index],
    ...payload,
  }

  console.log(newData);
  fs.writeFile('./data/data.json', JSON.stringify(newData), "utf-8", (err) => {
    if(err) throw err;
    console.log('Data Successfully Saved');
});
}

const deletedatakubus=(id)=>{
    const index=data.findIndex((row)=>row.id === id); //mencari index
    data.splice(index, 1);
  
    // console.log(data);
    fs.writeFile('./data/data.json', JSON.stringify(data), "utf-8", (err) => {
      if(err) throw err;
      console.log('Data Successfully Deleted');
  });
  
}


const tampildatakubusbyid=(id)=>{

    const index=data.findIndex((row)=>row.id===id); //mencari index
    console.log(data[index])
  
  }
  
  const tampildatakubus=()=>{
    
    const NewData= data.filter((row) =>row.jenis == "Kubus")
    console.log(NewData);
  
  }


module.exports = {luaskubus,volumekubus,editdatakubus,deletedatakubus,tampildatakubusbyid,tampildatakubus};

