const fs = require('fs');
const data = require('../data/data.json')

let luaslimas=(alas,tinggisegitigaalas)=>{
    const nilailuaslimas=(4*(1/2*alas*tinggisegitigaalas));
    simpandatalimas(nilailuaslimas,'Luas','Limas');
    return nilailuaslimas;
}

let volumelimas=(alas,tinggisegitigaalas,tinggilimas)=>{
    const nilaivolumelimas=(1/3*(1/2*alas*tinggisegitigaalas)*tinggilimas);
    simpandatalimas(nilaivolumelimas,'Volume','Limas');
    return nilaivolumelimas;
}

let simpandatalimas = (nilai,tipe,jenis) =>{
      let formdata={id:data.length,nilai,tipe,jenis}
      let newData = data;
      newData.push(formdata);
      // console.log(newData);
      fs.writeFile('./data/data.json', JSON.stringify(newData), "utf-8", (err) => {
          if(err) throw err;
          console.log('Data Successfully Saved');
      });
};

const editdatalimas=(param)=>{
  const{id, payload}=param;
  let newData=data;
  const index=newData.findIndex((row)=>row.id === id);

  newData[index]={
    ...newData[index],
    ...payload,
  }

  console.log(newData);
  fs.writeFile('./data/data.json', JSON.stringify(newData), "utf-8", (err) => {
    if(err) throw err;
    console.log('Data Successfully Saved');
});
}

const deletedatalimas=(id)=>{

  const index=data.findIndex((row)=>row.id === id); //mencari index
  data.splice(index, 1);

  // console.log(data);
  fs.writeFile('./data/data.json', JSON.stringify(data), "utf-8", (err) => {
    if(err) throw err;
    console.log('Data Successfully Deleted');
});
}

const tampildatalimasbyid=(id)=>{

  const index=data.findIndex((row)=>row.id===id); //mencari index
  console.log(data[index])

}

const tampildatalimas=()=>{
  
  const NewData= data.filter((row) =>row.jenis == "Limas")
  console.log(NewData);

}

module.exports = {luaslimas,volumelimas,editdatalimas,deletedatalimas,tampildatalimasbyid,tampildatalimas};

