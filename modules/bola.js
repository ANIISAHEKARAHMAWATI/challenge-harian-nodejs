const fs = require('fs');
const data = require('../data/data.json')

let luasbola=(r)=>{
    const nilailuasbola=(4*3.14*r*r);
    simpandatabola(nilailuasbola,'Luas','Bola');
    return nilailuasbola;
}

let volumebola=(r)=>{
    const nilaivolumebola=(4/3*3.14*r*r*r);
    simpandatabola(nilaivolumebola,'Volume','Bola');
    return nilaivolumebola;
}


let simpandatabola = (nilai,tipe,jenis) =>{
    let formdata={id:data.length,nilai,tipe,jenis}
    let newData = data;
    newData.push(formdata);
    console.log(newData);
    fs.writeFile('./data/data.json', JSON.stringify(newData), "utf-8", (err) => {
        if(err) throw err;
        console.log('Data Successfully Saved');
    });
};

const editdatabola=(param)=>{
  const{id, payload}=param;
  let newData=data;
  const index=newData.findIndex((row)=>row.id === id);

  newData[index]={
    ...newData[index],
    ...payload,
  }

  console.log(newData);
  fs.writeFile('./data/data.json', JSON.stringify(newData), "utf-8", (err) => {
    if(err) throw err;
    console.log('Data Successfully Saved');
});
}

const deletedatabola=(id)=>{
    const index=data.findIndex((row)=>row.id === id); //mencari index
    data.splice(index, 1);
  
    // console.log(data);
    fs.writeFile('./data/data.json', JSON.stringify(data), "utf-8", (err) => {
      if(err) throw err;
      console.log('Data Successfully Deleted');
  });
  
}

const tampildatabolabyid=(id)=>{

    const index=data.findIndex((row)=>row.id===id); //mencari index
    console.log(data[index])
  
  }
  
  const tampildatabola=()=>{
    
    const NewData= data.filter((row) =>row.jenis == "Bola")
    console.log(NewData);
  
  }


module.exports = {luasbola,volumebola,editdatabola,deletedatabola,tampildatabolabyid,tampildatabola};

